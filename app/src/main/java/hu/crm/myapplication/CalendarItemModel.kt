package hu.crm.myapplication

import java.util.*

class CalendarItemModel(
    var dateCalendar: Calendar,
    var countOfTasks: Int = 0,
    var countOfEvents: Int = 0,
    var numberOfActualMonth: Int = 0
) {
    fun getTaskCountAsString() : String{
        return countOfTasks.toString()
    }

    fun getEventCountAsString() : String{
        return countOfEvents.toString()
    }
}