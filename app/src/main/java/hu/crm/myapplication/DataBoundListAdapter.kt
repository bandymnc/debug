package hu.crm.myapplication

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

abstract class DataBoundListAdapter<T, V : ViewDataBinding>(diffCallback: DiffUtil.ItemCallback<T>) :
    ListAdapter<T, DataBoundVH<V>>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBoundVH<V> {
        val binding = createBinding(parent, viewType)
        return DataBoundVH(binding)
    }

    override fun onBindViewHolder(holder: DataBoundVH<V>, position: Int) {
        bind(holder.binding, getItem(position), holder.itemViewType)
        holder.binding.executePendingBindings()
    }

    public override fun getItem(position: Int): T {
        return super.getItem(position)
    }

    protected abstract fun createBinding(parent: ViewGroup, viewType: Int): V
    protected abstract fun bind(binding: V, item: T?, viewType: Int)
}