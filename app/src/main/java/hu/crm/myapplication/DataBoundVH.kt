package hu.crm.myapplication

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

class DataBoundVH<T : ViewDataBinding>(val binding: T) : RecyclerView.ViewHolder(binding.root)