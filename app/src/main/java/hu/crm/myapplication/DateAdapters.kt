package hu.crm.myapplication

import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.text.SimpleDateFormat
import java.util.*

@BindingAdapter("makeDateTimeString")
fun makeDateTimeString(textView: TextView, date: Date?) {
    if (date == null) return
    val dateTimeString: String
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

    dateTimeString = simpleDateFormat.format(date)
    textView.text = dateTimeString
}

@BindingAdapter("makeDateString")
fun makeDateString(textView: TextView, date: Date?) {
    if (date == null) return
    val dateTimeString: String
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

    dateTimeString = simpleDateFormat.format(date)
    textView.text = dateTimeString
}

@BindingAdapter("makeCalendarDayString")
fun makeDateDayString(textView: TextView, calendar: Calendar?){
    if (calendar == null) return

    textView.text = calendar.get(Calendar.DAY_OF_MONTH).toString()
}