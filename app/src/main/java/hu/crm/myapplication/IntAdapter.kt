package hu.crm.myapplication

import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("setUpMonth")
fun setUpMonth(textView: TextView, numberOfMonth: Int){
    var monthString = ""

    when(numberOfMonth){
        1-> monthString = "Január"
        2-> monthString = "Február"
        3-> monthString = "Március"
        4-> monthString = "Április"
        5-> monthString = "Május"
        6-> monthString = "Június"
        7-> monthString = "Július"
        8-> monthString = "Augusztus"
        9-> monthString = "Szeptember"
        10-> monthString = "Október"
        11-> monthString = "November"
        12-> monthString = "December"
    }

    textView.text = monthString
}

@BindingAdapter("getStringFromInt")
fun getStringFromInt(textView: TextView, number: Int){
    textView.text = number.toString()
}