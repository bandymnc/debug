package hu.crm.myapplication

import android.animation.ObjectAnimator
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.view.View
import androidx.databinding.BindingAdapter
import com.google.android.material.card.MaterialCardView
import com.google.android.material.floatingactionbutton.FloatingActionButton

object ViewAdapters {
    /**
     * View megjelenítése/elrejtése
     * @param view view
     * @param show látható legyen-e a view
     */
    @JvmStatic
    @BindingAdapter("showHide")
    fun showHide(view: View, show: Boolean?) {

        val showValue = show ?: false

        if (view is FloatingActionButton) {
            if (showValue) view.show()
            else view.hide()
        } else {
            view.visibility = if (showValue) View.VISIBLE else View.GONE
        }
    }

    /**
     * View megjelenítése/elrejtése
     * @param view view
     * @param show látható legyen-e a view
     */
    @JvmStatic
    @BindingAdapter("showInvisible")
    fun showInvisible(view: View, show: Boolean?) {
        val showValue = show ?: false
        view.visibility = if (showValue) View.VISIBLE else View.INVISIBLE

    }
}