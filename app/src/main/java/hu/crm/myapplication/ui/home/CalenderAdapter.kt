package hu.crm.myapplication.ui.home

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import hu.crm.myapplication.CalendarItemModel
import hu.crm.myapplication.DataBoundListAdapter
import hu.crm.myapplication.databinding.CalendarFragmentListItemBinding
import java.util.*

class CalenderAdapter(
    private val dateItemClick: ((calendar: Calendar) -> Unit)
) : DataBoundListAdapter<CalendarItemModel, CalendarFragmentListItemBinding>(DIFF_CALLBACK) {

    override fun createBinding(
        parent: ViewGroup,
        viewType: Int
    ): CalendarFragmentListItemBinding {
        return CalendarFragmentListItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
    }

    override fun bind(binding: CalendarFragmentListItemBinding, item: CalendarItemModel?, viewType: Int) {
        binding.model = item

        item?.let { model->
            binding.calendarFragmentListItemCardView.setOnClickListener {
                dateItemClick.invoke(model.dateCalendar)
            }

        }
    }

    companion object {
        val DIFF_CALLBACK = object: DiffUtil.ItemCallback<CalendarItemModel>() {
            override fun areItemsTheSame(oldItem: CalendarItemModel, newItem: CalendarItemModel): Boolean {
                return oldItem.dateCalendar.timeInMillis == newItem.dateCalendar.timeInMillis
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: CalendarItemModel, newItem: CalendarItemModel): Boolean {
                return oldItem == newItem
            }
        }
    }
}