package hu.crm.myapplication.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import hu.crm.myapplication.CalendarItemModel
import hu.crm.myapplication.R
import hu.crm.myapplication.databinding.FragmentHomeBinding
import kotlinx.coroutines.flow.combine
import java.util.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var adapter: CalenderAdapter
    private var actualNumberOfMonth = 1
    private var actualYear = 0
    private lateinit var binding : FragmentHomeBinding


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        //0-ról kezdi a számozást a Calendar
        actualNumberOfMonth = Calendar.getInstance().get(Calendar.MONTH) + 1
        actualYear = Calendar.getInstance().get(Calendar.YEAR)

        adapter = CalenderAdapter { dateClick(it) }

        initRecyclerView()
        initListeners()
        handleButtons()
    }

    private fun dateClick(calendar: Calendar) {
        //view?.showSnackBar("Kattintás a ${calendar.time} elemre")
    }

    private fun initRecyclerView() {
        adapter.submitList(createMonthDateList())

        binding.fragmentCalendarRecyclerView.layoutManager = GridLayoutManager(context, 7)
        binding.fragmentCalendarRecyclerView.adapter = adapter
    }

    private fun initListeners() {
        binding.fragmentCalendarNextButton.setOnClickListener {
            actualNumberOfMonth++
            handleButtons()
        }

        binding.fragmentCalendarBackButton.setOnClickListener {
            actualNumberOfMonth--
            handleButtons()
        }
    }

    private fun handleButtons() {
        if (actualNumberOfMonth < 1) {
            actualNumberOfMonth = 12
            actualYear--
        }

        if (actualNumberOfMonth > 12) {
            actualNumberOfMonth = 1
            actualYear++
        }

        binding.numberOfMonth = actualNumberOfMonth
        binding.year = actualYear
        adapter.submitList(createMonthDateList())
    }

    private fun createMonthDateList(): List<CalendarItemModel> {
        val models: MutableList<CalendarItemModel> = mutableListOf()

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR,0)
        calendar.set(Calendar.MINUTE,0)
        calendar.set(Calendar.MILLISECOND,0)
        calendar.set(Calendar.SECOND,0)

        calendar.set(Calendar.MONTH, actualNumberOfMonth - 1)
        calendar.set(Calendar.YEAR, actualYear)
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        var dateNumberOfWeek = calendar[Calendar.DAY_OF_WEEK] - 1
        dateNumberOfWeek = 1 - dateNumberOfWeek

        calendar.add(Calendar.DAY_OF_MONTH, dateNumberOfWeek)

        while (models.size < 35) {
            models.add(CalendarItemModel(calendar.clone() as Calendar, models.size, models.size,actualNumberOfMonth))
            calendar.add(Calendar.DAY_OF_MONTH, 1)
        }

        return models
    }
}